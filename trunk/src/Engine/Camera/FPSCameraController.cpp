#include "FPSCameraController.h"
#include "Debug\imgui.h"

namespace engine
{

	CFPSCameraController::CFPSCameraController(Vect3f center, float maxPitch, float minPitch, float maxZoom, float minZoom) :
		maxPitch(maxPitch), minPitch(minPitch), maxZoom(maxZoom), noZoom(minZoom)
	{
		pitch = 0.2f;
		yaw = 0.0f;
		zoom = 10.0f;
		m_Position = center;

		yawSpeed = 0.0f;
		pitchSpeed = 0.0f;
		zoomSpeed = 0.0f;
		frontSpeed = 0.0f;
		sideSpeed = 0.0f;
	}

	CFPSCameraController::~CFPSCameraController()
	{

	}

	void CFPSCameraController::Update(float ElapsedTime)
	{
		yaw += yawSpeed * ElapsedTime;
		pitch += pitchSpeed * ElapsedTime;
		zoom -= zoomSpeed * ElapsedTime;

		if (pitch > maxPitch)
			pitch = maxPitch;
		if (pitch < minPitch)
			pitch = minPitch;

		if (zoom > maxZoom)
			zoom = maxZoom;
		if (zoom < noZoom)
			zoom = noZoom;

		if (yaw > mathUtils::PiTimes<float>())
			yaw -= mathUtils::PiTimes<float>(2.0f);
		if (yaw < -mathUtils::PiTimes<float>())
			yaw += mathUtils::PiTimes<float>(2.0f);

		float deltaX = (cos(yaw) * cos(pitch) * frontSpeed) + (sin(yaw) * sideSpeed);
		float deltaY = sin(-pitch) * frontSpeed;
		float deltaZ = (sin(yaw) * cos(pitch) * frontSpeed) + (-cos(yaw) * sideSpeed);

		m_Front.z = sin(yaw) * cos(pitch);
		m_Front.y = -sin(pitch);
		m_Front.x = cos(yaw) * cos(pitch);

		m_Position.x += deltaX;
		m_Position.y += deltaY;
		m_Position.z += deltaZ;

		yawSpeed = 0.0f;
		pitchSpeed = 0.0f;
		zoomSpeed = 0.0f;
		frontSpeed = 0.0f;
		sideSpeed = 0.0f;
	}

	void CFPSCameraController::RenderDebugGUI()
	{
		ImGui::Text("FPS Camera");
		if (ImGui::CollapsingHeader("Mouse / Keyboard controls"))
		{
			ImGui::Text("Right click and drag to rotate");
			ImGui::Text("WASD / Directional arrows to move");
		}
		if (ImGui::CollapsingHeader("Gamepad controls"))
		{
			ImGui::Text("Right stick to rotate");
			ImGui::Text("Left stick to move");
		}
		ImGui::Separator();
		ImGui::SliderFloat("YAW", &yaw, -mathUtils::PiTimes<float>(), mathUtils::PiTimes<float>());
		ImGui::SliderFloat("PITCH", &pitch, minPitch, maxPitch);
		ImGui::Separator();
		ImGui::SliderFloat("X", &m_Position.x, -20.0f, 20.0f);
		ImGui::SliderFloat("Y", &m_Position.y, -20.0f, 20.0f);
		ImGui::SliderFloat("Z", &m_Position.z, -20.0f, 20.0f);
	}

	void CFPSCameraController::HandleActions(CActionManager *actionManager)
	{
		if (actionManager->Get("ROTATE")->active)
		{
			if (actionManager->Get("YAW")->active)
			{
				yawSpeed = actionManager->Get("YAW")->value;
			}
			if (actionManager->Get("PITCH")->active)
			{
				pitchSpeed = actionManager->Get("PITCH")->value;
			}
		}

		if (actionManager->Get("FRONT")->active)
		{
			frontSpeed = actionManager->Get("FRONT")->value;
		}
		if (actionManager->Get("SIDE")->active)
		{
			sideSpeed = actionManager->Get("SIDE")->value;
		}
	}

}