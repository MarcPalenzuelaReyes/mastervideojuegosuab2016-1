#include "CameraController.h"

namespace engine
{

	class CFPSCameraController : public CCameraController
	{
	public:
		float yawSpeed, pitchSpeed;
		float zoomSpeed, frontSpeed, sideSpeed;

		CFPSCameraController(Vect3f center = Vect3f(0.0f, 0.0f, 0.0f), float maxPitch = 1.5f, float minPitch = -1.5f, float maxZoom = 20.0f, float noZoom = 1.0f);
		virtual ~CFPSCameraController();

		virtual void Update(float ElapsedTime) override;
		virtual void RenderDebugGUI() override;
		virtual void HandleActions(CActionManager *actionManager) override;

	private:
		float yaw, pitch, zoom;

		float maxPitch, minPitch;
		float maxZoom, noZoom;
	};

}