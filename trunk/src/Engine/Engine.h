#pragma once

#include <Windows.h>
#include <chrono>
#include "Utils\Singleton.h"

#define BUILD_GET_SET_ENGINE_MANAGER( Manager ) \
private: \
C##Manager* m_##Manager = nullptr; \
public: \
void Set##Manager(C##Manager* a##Manager) { m_##Manager = a##Manager;  } \
const C##Manager& Get##Manager() const { return *m_##Manager; } \
C##Manager& Get##Manager() { return *m_##Manager; } \
bool Has##Manager() { return m_##Manager != nullptr; } \

namespace engine
{
	class CActionManager;
	class CRenderManager;
	class CCameraController;
	class CEngine : public base::utils::CSingleton<CEngine>
	{
	public:
		
		virtual ~CEngine();

		void ProcessInputs();
		void Update();
		void Render();
		void Init(HWND hWnd);

		BUILD_GET_SET_ENGINE_MANAGER(ActionManager);
		BUILD_GET_SET_ENGINE_MANAGER(RenderManager);
		BUILD_GET_SET_ENGINE_MANAGER(CameraController);

	protected:
		CEngine();

		friend class base::utils::CSingleton<CEngine>;

	private:
		std::chrono::monotonic_clock m_Clock;
		std::chrono::monotonic_clock::time_point m_PrevTime;

		// Temporal
		float scale = 1.0f;
		float rotation = 0.0f;
		int selection = 1;

		// ImGui
		bool m_ShowCameraDebug = true;
		bool m_ShowGeneralDebug = true;
		bool m_ShowRenderOptions = true;

		void DebugUI();
	};
}

#undef BUILD_GET_SET_ENGINE_MANAGER