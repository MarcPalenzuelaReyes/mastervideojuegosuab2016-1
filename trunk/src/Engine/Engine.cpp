#include "Engine.h"

#include "Input\ActionManager.h"
#include "RenderManager.h"
#include "Camera\SphericalCameraController.h"
#include "Camera\FPSCameraController.h"
#include "Camera\TPSCameraController.h"
#include "Debug\imgui.h"
#include "Debug\imgui_impl_dx11.h"
#ifdef _DEBUG
#include "Utils\MemLeaks\MemLeaks.h"
#endif



namespace engine
{
	CEngine::CEngine() : m_RenderManager(nullptr)
		, m_ActionManager(nullptr)
		, m_CameraController(nullptr)
		, m_Clock()
		, m_PrevTime(m_Clock.now())
	{
		
	}

	void CEngine::Init(HWND hWnd)
	{
		ImGui_ImplDX11_Init(hWnd, m_RenderManager->GetDevice(), m_RenderManager->GetDeviceContext());
	}

	CEngine::~CEngine()
	{

		delete m_ActionManager;
		delete m_RenderManager;
		delete m_CameraController;
		ImGui_ImplDX11_Shutdown();
	}

	void CEngine::ProcessInputs()
	{
		ImGui_ImplDX11_NewFrame();
		m_ActionManager->Update();
		m_CameraController->HandleActions(m_ActionManager);
	}

	void CEngine::Update()
	{
		// EMPIEZA CALCULO DELTA TIME
		auto currentTime = m_Clock.now();
		std::chrono::duration<float> chronoDeltaTime = currentTime - m_PrevTime;
		m_PrevTime = currentTime;

		float dt = chronoDeltaTime.count() > 0.5f ? 0.5f : chronoDeltaTime.count();
		// TERMINA CALCULO DELTA TIME

		Vect3f spherePos = Vect3f(0.0f, 0.0f, 0.0f);
		if (m_ActionManager->Get("SELECTCAMERAMODE")->active) {
			if (m_ActionManager->Get("FIGURE")->active)
			{
				engine::CCameraController *cc;
				switch ((int)m_ActionManager->Get("FIGURE")->value)
				{
					case 0:
						cc = new engine::CSphericalCameraController(Vect3f(0.0f, 0.0f, 0.0f), 1.5f, -1.5f, 20.0f, 1.0f);
						break;
					case 1:
						cc = new engine::CFPSCameraController(Vect3f(-3.5f, 1.0f, 0.0f), 1.5f, -1.5f, 20.0f, 1.0f);
						break;
					case 2:
						cc = new engine::CTPSCameraController();
						break;
					default:
						cc = new engine::CSphericalCameraController(Vect3f(0.0f, 0.0f, 0.0f), 1.5f, -1.5f, 20.0f, 1.0f);
						break;
				}
				SetCameraController(cc);
			}
		}
		else 
		{
			if (m_ActionManager->Get("FIGURE")->active)
			{
				selection = (int)m_ActionManager->Get("FIGURE")->value;
			}
		}
		


		m_CameraController->Update(dt);
		m_CameraController->SetToRenderManager(*m_RenderManager);
	}

	void CEngine::Render()
	{
		m_RenderManager->BeginRender();

		switch (selection)
		{
			case 0:
				m_RenderManager->DrawSphere(scale, rotation, CColor(1.0f, 1.0f, 1.0f));
				m_RenderManager->DrawAxis(scale, scale, scale, rotation, CColor(1.0f, 1.0f, 1.0f));
				break;
			case 1:
				m_RenderManager->DrawGrid(scale, scale, scale, rotation, CColor(1.0f, 1.0f, 1.0f));
				break;
			case 2:
				m_RenderManager->DrawCube(scale, scale, scale, rotation, CColor(1.0f, 1.0f, 1.0f));
				break;
		}
		m_CameraController->Render(m_RenderManager);

		DebugUI();
		ImGui::Render();
		m_RenderManager->EndRender();
	}

	void CEngine::DebugUI()
	{
		bool show = true;
		ImGui::Begin("Debug", &show, ImGuiWindowFlags_AlwaysAutoResize);
		if (ImGui::Button("General Settings")) {
			m_ShowGeneralDebug = !m_ShowGeneralDebug;
		}
		if (ImGui::Button("Camera Settings")) {
			m_ShowCameraDebug = !m_ShowCameraDebug;
		}
		if (ImGui::Button("Render Settings")) {
			m_ShowRenderOptions = !m_ShowRenderOptions;
		}
		ImGui::End();

		if (m_ShowRenderOptions) {
			ImGui::Begin("Render Options", &m_ShowRenderOptions, ImGuiWindowFlags_AlwaysAutoResize);
			if (ImGui::Button("Render grid")) {
				selection = 1;
			}
			if (ImGui::Button("Render cube")) {
				selection = 2;
			}
			if (ImGui::Button("Render sphere")) {
				selection = 0;
			}
			ImGui::End();
		}

		if (m_ShowGeneralDebug) {
			ImGui::Begin("General debug", &m_ShowGeneralDebug, ImGuiWindowFlags_AlwaysAutoResize);
			ImGui::Text("FPS: %d", (int) ImGui::GetIO().Framerate);
			for (int i = 0; i < MAX_PLAYERS; i++) {
				if (m_ActionManager->IsGamepadConnected(i))
				{
					ImGui::Text("Player %d: Gamepad connected", i+1);
				}
				else
				{
					ImGui::Text("Player %d: Gamepad not connected", i+1);
				}
			}
			
			if (ImGui::Button("FPS Camera")) {
				engine::CCameraController *cc;
				cc = new engine::CFPSCameraController(Vect3f(-3.5f, 1.0f, 0.0f), 1.5f, -1.5f, 20.0f, 1.0f);
				SetCameraController(cc);
			}
			ImGui::SameLine();
			if (ImGui::Button("Spherical Camera")) {
				engine::CCameraController *cc;
				cc = new engine::CSphericalCameraController(Vect3f(0.0f, 0.0f, 0.0f), 1.5f, -1.5f, 20.0f, 1.0f);
				SetCameraController(cc);
			}
			ImGui::SameLine();
			if (ImGui::Button("TPS Camera")) {
				engine::CCameraController *cc;
				cc = new engine::CTPSCameraController();
				SetCameraController(cc);
			}
			ImGui::End();
		}
		
		if (m_ShowCameraDebug) {
			ImGui::Begin("Camera Settings", &m_ShowCameraDebug, ImGuiWindowFlags_AlwaysAutoResize);
			m_CameraController->RenderDebugGUI();
			ImGui::End();
		}
	}
}