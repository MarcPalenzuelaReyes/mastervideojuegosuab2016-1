#pragma once

#include <vector>

#include "Utils\TemplatedMap.h"
#include "ActionTrigger.h"
#include "InputManager.h"

namespace engine
{

	struct InputAction
	{
		float value;
		bool active;
		int player;
		std::vector<ActionTrigger> triggers;
	};

	class CActionManager : public base::utils::CTemplatedMap<InputAction>
	{
		public:
			~CActionManager();
			void Update();
			void InitInputManager(const HWND &hWnd);
			bool HandleKeyboard(const MSG& msg);
			bool HandleMouse(const MSG& msg);
			void PreUpdate(bool windowIsActive);
			void PostUpdate();
			void SetTestActions();
			void LoadActions(const char* path);
			unsigned char GetKeyCode(const char *string);
			bool IsGamepadConnected(int player);
		private:
			CInputManager m_InputManager;
			
		
	};

}