// No puedo hacer CTRL + Numero!

#include <Windows.h>

#include "Utils\TemplatedMap.h"
#ifdef _DEBUG
#include "Utils\MemLeaks\MemLeaks.h"
#endif
#include "Engine.h"
#include "RenderManager.h"
#include "Input\ActionManager.h"
#include "Camera\SphericalCameraController.h"
#include "Camera\FPSCameraController.h"

#define APPLICATION_NAME	"VIDEOGAME"

#define DEFAULT_WIDTH 1600
#define DEFAULT_HEIGHT 900

int WIDTH = DEFAULT_WIDTH;
int HEIGHT = DEFAULT_HEIGHT;
WINDOWPLACEMENT LAST_WINDOW = { sizeof(WINDOWPLACEMENT) }; // MPR - 26/11/2016 - �ltima posici�n de la ventana

bool s_WindowActive;

void ToggleFullscreen(HWND Window, WINDOWPLACEMENT &WindowPosition)
{
	// This follows Raymond Chen's prescription
	// for fullscreen toggling, see:
	// http://blogs.msdn.com/b/oldnewthing/archive/2010/04/12/9994016.aspx

	DWORD Style = GetWindowLongW(Window, GWL_STYLE);
	if (Style & WS_OVERLAPPEDWINDOW)
	{
		LAST_WINDOW = WindowPosition; // MPR - 26/11/2016 - Guardamos la �ltima posici�n de la ventana
		MONITORINFO MonitorInfo = { sizeof(MonitorInfo) };
		if (GetWindowPlacement(Window, &WindowPosition) &&
			GetMonitorInfoW(MonitorFromWindow(Window, MONITOR_DEFAULTTOPRIMARY), &MonitorInfo))
		{
			SetWindowLongW(Window, GWL_STYLE, Style & ~WS_OVERLAPPEDWINDOW);
			SetWindowPos(Window, HWND_TOP,
				MonitorInfo.rcMonitor.left, MonitorInfo.rcMonitor.top,
				MonitorInfo.rcMonitor.right - MonitorInfo.rcMonitor.left,
				MonitorInfo.rcMonitor.bottom - MonitorInfo.rcMonitor.top,
				SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
		}
	}
	else
	{
		SetWindowLongW(Window, GWL_STYLE, Style | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(Window, &LAST_WINDOW); // MPR - 26/11/2016 - Aplicamos la �ltima posici�n de la ventana
		SetWindowPos(Window, 0, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER |
			SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
		
	}
}

//-----------------------------------------------------------------------------
// Name: MsgProc()
// Desc: The window's message handler
//-----------------------------------------------------------------------------
LRESULT WINAPI MsgProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{

  switch( msg )
  {
  case WM_DESTROY:
    {
      PostQuitMessage( 0 );
      return 0;
    }
    break;
  case WM_KEYDOWN:
    {
      switch( wParam )
      {
      case VK_ESCAPE:
        //Cleanup();
        PostQuitMessage( 0 );
        return 0;
        break;
      }
    }
    break;
  case WM_SIZE:
  {
	  if (wParam != SIZE_MINIMIZED)
	  {
		  if (engine::CEngine::GetInstance().HasRenderManager())
		  {
			  auto& rm = engine::CEngine::GetInstance().GetRenderManager();
			  WIDTH = (UINT)LOWORD(lParam);
			  HEIGHT = (UINT)HIWORD(lParam);
			  rm.Resize(WIDTH, HEIGHT);
			  engine::CEngine::GetInstance().Init(hWnd);
		  }
	  }
  }
  break;
  case WM_ACTIVATE:
	  s_WindowActive = wParam != WA_INACTIVE;
  }//end switch( msg )

  return DefWindowProc( hWnd, msg, wParam, lParam );
}

extern LRESULT ImGui_ImplDX11_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

//-----------------------------------------------------------------------
// WinMain
//-----------------------------------------------------------------------
int APIENTRY WinMain(HINSTANCE _hInstance, HINSTANCE _hPrevInstance, LPSTR _lpCmdLine, int _nCmdShow)
{
#ifdef _DEBUG
	MemLeaks::MemoryBegin();
#endif


  // Register the window class
  WNDCLASSEX wc = {	sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L, GetModuleHandle(NULL), NULL, NULL, NULL, NULL, APPLICATION_NAME, NULL };
  
  RegisterClassEx( &wc ); 

  RECT rc = { 0, 0, WIDTH, HEIGHT };
  AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);

  // Create the application's window
  HWND hWnd = CreateWindow(APPLICATION_NAME, APPLICATION_NAME, WS_OVERLAPPEDWINDOW, 100, 100, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, wc.hInstance, NULL);

  // A�adir aqu� el Init de la applicacio�n

  ShowWindow( hWnd, SW_SHOWDEFAULT );
  UpdateWindow( hWnd );
  MSG msg;
  ZeroMemory( &msg, sizeof(msg) );

  // A�adir en el while la condici�n de salida del programa de la aplicaci�n
  // Creamos engine
  engine::CEngine& engine = engine::CEngine::GetInstance();

  // A�adimos action manager
  engine::CActionManager *l_ActionManager = new engine::CActionManager();
  l_ActionManager->InitInputManager(hWnd);
  l_ActionManager->LoadActions("data/actions.xml");
  engine.SetActionManager(l_ActionManager);

  // A�adimos render manager
  engine::CRenderManager *rm = new engine::CRenderManager();
  rm->Init(hWnd, WIDTH, HEIGHT);
  rm->SetModelMatrix(Mat44f().SetIdentity());
  rm->SetProjectionMatrix(0.8f, (float)WIDTH / (float)HEIGHT, 0.5f, 100.0f);
  rm->SetViewMatrix(Vect3f(5.0f, 2.0f, 3.5f), Vect3f(0.0f, 0.0f, 0.0f), Vect3f(0.0f, 1.0f, 0.0f));
  engine.SetRenderManager(rm);

  // A�adimos camara  
  engine::CCameraController *cc = new engine::CFPSCameraController(Vect3f(-3.5f, 1.0f, 0.0f), 1.5f, -1.5f, 20.0f, 1.0f);
  engine.SetCameraController(cc);

  engine.Init(hWnd);
  
  bool fHandled = false;
  bool toggle = false;

  while(msg.message != WM_QUIT)
  {
	  toggle = false;
	l_ActionManager->PreUpdate(s_WindowActive);
	fHandled = false;
    while( PeekMessage( &msg, NULL, 0U, 0U, PM_REMOVE ) )
    {
		// Inicializamos ImGui
		ImGui_ImplDX11_WndProcHandler(hWnd, msg.message, msg.wParam, msg.lParam);
		TranslateMessage( &msg );
		DispatchMessage( &msg );

		if ((msg.message >= WM_MOUSEFIRST && msg.message <= WM_MOUSELAST) || msg.message == WM_INPUT)
		{
			fHandled = l_ActionManager->HandleMouse(msg);
		}
		if (msg.message >= WM_KEYFIRST && msg.message <= WM_KEYLAST)
		{
			fHandled = l_ActionManager->HandleKeyboard(msg);
		}

		bool WasDown = ((msg.lParam & (1 << 30)) != 0);
		bool IsDown = ((msg.lParam & (1 << 31)) == 0);
		bool Alt = ((msg.lParam & (1 << 29)) != 0);
		if (!WasDown && IsDown && Alt && msg.wParam == VK_RETURN && !toggle)
		{
			WINDOWPLACEMENT windowPosition = { sizeof(WINDOWPLACEMENT) };
			GetWindowPlacement(msg.hwnd, &windowPosition);

			ToggleFullscreen(msg.hwnd, windowPosition);
			fHandled = true;
			toggle = true;
		}

		if (msg.message == WM_QUIT)
			break;
    }
	l_ActionManager->PostUpdate();
	
    
    {
		engine.ProcessInputs();
		engine.Update();
		engine.Render();		
    }
  }
  
  UnregisterClass(APPLICATION_NAME, wc.hInstance);

  // A�adir una llamada a la alicaci�n para finalizar/liberar memoria de todos sus datos

#ifdef _DEBUG
  MemLeaks::MemoryEnd();
#endif
  return 0;
}
