/// <reference path="C:\Users\Luis.Ruiz\AppData\Local\Microsoft\VisualStudio\12.0\Macros\dte.js" />

var doc = dte.ActiveDocument;

var filename = doc.Name.substr(0, doc.Name.lastIndexOf("."));

var date = new Date();

var day = date.getDate();
var month = date.getMonth() + 1;
var year = date.getYear();

var hours = date.getHours();
var minutes = date.getMinutes();

var seconds = date.getSeconds();

// Add a zero if single digit
if (day <= 9) day = "0" + day;
if (month <= 9) month = "0" + month;
if (minutes <= 9) minutes = "0" + minutes;
if (hours <= 9) hours = "0" + hours;

var project = doc.ProjectItem.ContainingProject.Name;

var define = ("_" + project + "_" + filename + "_" + day + month + year + hours + minutes + seconds + "_H").replace(/\s/g, '').toUpperCase();

if (dte.UndoContext.IsOpen)
{
    dte.UndoContext.Close();
}

dte.UndoContext.Open("One Time Include");
    doc.Selection.StartOfDocument(false);

    doc.Selection.Insert("#ifndef " + define, 1);
    doc.Selection.NewLine();
    doc.Selection.Insert("#define " + define, 1);

    doc.Selection.NewLine();

    doc.Selection.EndOfDocument(false);

    doc.Selection.NewLine();
    doc.Selection.Insert("#endif	// " + define, 1);
dte.UndoContext.Close();